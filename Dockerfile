FROM node:14

COPY ./source /usr/src/front

WORKDIR /usr/src/front

RUN npm install

EXPOSE 4200/tcp

CMD npm run start -- --host 0.0.0.0 --disableHostCheck
