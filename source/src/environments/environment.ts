export const environment = {
    apiBaseUrl: 'http://backend-dev.openshift.devops.t-systems.ru',
    clientBaseUrl: 'http://localhost:4200',
    production: true
};
